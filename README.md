# Text_Classification_TF-IDF

Project goal is to build a text classifier for the news articles using labeled dataset of 200.000 HuffPost articles in 41 different category.

There is a high probability, that some of the classes might be similar and overlaping, so the model will have a better accuracy if the prediction for more than one class will be compared to the true class.

Dataset is from - https://www.kaggle.com/rmisra/news-category-dataset/data